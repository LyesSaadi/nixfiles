{ pkgs, ... }:

{
  programs.steam = {
    enable = true;
    gamescopeSession = {
      enable = true;
      args = [
        "--rt"
        # "--mangoapp"
        # "--hdr-enabled"
        "--adaptive-sync"
      ];
      # env = {
      #   MANGOHUD = "1";
      #   MANGOHUD_CONFIG = "fps,cpu_temp,gpu_temp,ram,vram";
      # };
    };
    package = pkgs.steam.override {
      extraArgs = "-forcedesktopscaling 1.5";
    };
  };
  programs.gamemode.enable = true;

  environment.systemPackages = with pkgs; [
    wineWowPackages.waylandFull
    mangohud
    protonup
  ];
}
