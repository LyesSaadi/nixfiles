{ lib, pkgs, ... }:

{
  specialisation = {
    old-kernel.configuration = {
      boot.kernelPackages = lib.mkForce pkgs.linuxPackages_6_1;
    };
  };
}
