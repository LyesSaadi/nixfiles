{ lib, ... }:

{
  specialisation = {
    kde-gaming.configuration = {
      hardware.nvidia = {
        prime.sync.enable = lib.mkForce true;
        prime.offload = {
          enable = lib.mkForce false;
          enableOffloadCmd = lib.mkForce false;
        };
      };

      # GNOME
      services.xserver.displayManager.gdm.enable = lib.mkForce false;
      services.xserver.desktopManager.gnome.enable = lib.mkForce false;

      # KDE
      services.desktopManager.plasma6.enable = true;
      services.displayManager.sddm.enable = true;
    };
  };
}
