{ lib, ... }:

{
  specialisation = {
    kde.configuration = {
      # GNOME
      services.xserver.displayManager.gdm.enable = lib.mkForce false;
      services.xserver.desktopManager.gnome.enable = lib.mkForce false;

      # KDE
      services.desktopManager.plasma6.enable = true;
      services.displayManager.sddm.enable = true;
    };
  };
}
