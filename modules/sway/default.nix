{ pkgs, ... }:

let xwayland-sway = 
    (pkgs.writeScriptBin "Xwayland-sway" ''
          #! ${pkgs.bash}/bin/bash
          exec ${pkgs.xwayland}/bin/Xwayland "$@" -hidpi
        '');
in
{
  programs.sway = {
    enable = true;
    wrapperFeatures = {
      base = true;
      gtk = true;
    };
    extraSessionCommands = ''
      # SDL:
      export SDL_VIDEODRIVER=wayland
      # QT (needs qt5.qtwayland in systemPackages):
      export QT_QPA_PLATFORM=wayland-egl
      export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
      # Fix for some Java AWT applications (e.g. Android Studio),
      # use this if they aren't displayed properly:
      export _JAVA_AWT_WM_NONREPARENTING=1
      export WLR_XWAYLAND=${xwayland-sway}/bin/Xwayland-sway
    '';
    extraPackages = with pkgs; [
      brightnessctl
      grim
      swayidle
      swaylock
      wmenu
      mako
      waybar
      poweralertd
      lxde.lxsession
      networkmanagerapplet
      wofi
      wlogout
      slurp
    ];
  };

  environment.systemPackages = [
    xwayland-sway
  ];
}
