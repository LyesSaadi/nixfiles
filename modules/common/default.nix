{ nixpkgs-unstable, ... }:

{
  imports =
    [
      ./packages.nix
      ./system.nix
    ];

  # Import local packages
  nixpkgs.overlays = [
    (final: prev: {
      local = import ../../pkgs { pkgs = final; };
    })

    # Unstable
    (final: prev: {
      unstable = import nixpkgs-unstable {
        system = prev.system;
      };
    })
  ];

  nixpkgs.config = {
    allowUnfree = true;
    # allowBroken = true;
  };

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  nix.optimise.automatic = true;

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?
}
