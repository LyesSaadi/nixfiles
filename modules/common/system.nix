{ pkgs, ... }:

{
  # Time
  time.timeZone = "Europe/Paris";

  # Locale
  i18n.defaultLocale = "fr_FR.UTF-8";

  # Keyboard
  console = {
    font = "Lat2-Terminus16";
    # keyMap = "fr";
    useXkbConfig = true;
  };

  services.xserver.xkb.layout = "fr";
  services.xserver.xkb.variant = "oss";

  # Shell
  programs.fish.enable = true;
  programs.fish.promptInit = ''
    any-nix-shell fish --info-right | source
  '';
  environment.shells = with pkgs; [ fish ];

  # Environment Variables
  environment.sessionVariables = {
    EDITOR = "hx";
  };

  # Documentation
  documentation.enable = true;
  documentation.man.enable = true;
  documentation.dev.enable = true;
  # Fails for some reason
  documentation.nixos.enable = true;
}
