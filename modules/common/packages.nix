{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Utilities
    neovim
    helix
    wget
    ripgrep
    eza
    bat
    tree
    dust
    pciutils
    man-pages
    man-pages-posix
    python3
    any-nix-shell

    # Nix
    home-manager
    nix-index
    direnv
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
}
