{ pkgs, lib, config, zen-browser, ... }:

let
  tex = (pkgs.texlive.combine { inherit (pkgs.texlive)
      scheme-medium
      lettre
      wallpaper
      lastpage
      hyphenat
      moresize
      fontawesome5
      montserrat
      titlesec
      ly1
      footmisc
      titling
      frpseudocode
      ebproof
      comment;
    });
in {
  # Packages
  environment.systemPackages = with pkgs; [
    # Web
    zen-browser.packages."${system}".default
    fragments
    dropbox
    varia
    filezilla
    whatip
    epiphany
    organicmaps

    # Messaging
    thunderbird-128
    discord
    vesktop
    whatsapp-for-linux
    # element-desktop
    fractal
    zoom-us

    # Image
    drawing
    gimp
    loupe
    snapshot

    # Video
    mpv
    vlc
    syncplay
    obs-studio
    yt-dlp
    handbrake
    kooha
    video-trimmer
    (kodi.withPackages(kodiPkgs: with kodiPkgs; [
      youtube
      sponsorblock
      pvr-iptvsimple
    ]))

    # Audio
    shortwave
    gnome-podcasts
    helvum
    local.quadcastrgb
    gnome-sound-recorder
    easyeffects

    # Games
    heroic
    # cemu
    prismlauncher
    # vvvvvv
    ryujinx
    dolphin-emu
    parsec-bin

    # Reading
    # calibre
    papers

    # Office
    libreoffice
    onlyoffice-bin
    hunspell
    hunspellDicts.fr-reforme1990
    hunspellDicts.fr-any
    hunspellDicts.fr-moderne
    hunspellDicts.en_US
    hunspellDicts.en_GB-ize

    # Note taking
    apostrophe
    setzer
    tex
    pandoc
    unstable.typst
    tinymist
    zotero

    # Programming
    git
    git-lfs
    vscode
    # zed-editor
    gcc
    rustup
    python3
    ocaml
    ocamlPackages.ocaml-lsp
    ocamlPackages.ocamlformat
    opam
    ledit
    nodejs
    nil
    nixd
    nixfmt-rfc-style
    protege # LogIA Course
    haskellPackages.Agda
    agdaPackages.standard-library
    devtoolbox

    # Containers & VMs
    toolbox
    distrobox
    gnome-boxes

    # Virtualization
    qemu
    virt-manager

    # System
    gnome-firmware

    # Utilities
    ghostty
    impression
    resources
    file-roller
    baobab
    planify
    dosage-tracker

    # Proton
    protonmail-bridge-gui
    protonmail-desktop
    protonvpn-gui
    proton-pass

    # Customization
    gnome-tweaks
    gnome-extension-manager
    adw-gtk3
    paper-icon-theme
    # gnomeExtensions.gsconnect

    # Multimedia
    pipewire.dev
    # pulseaudio
    libopus

    # Printing
    hplipWithPlugin
  ];

  # Flatpaks
  services.flatpak = lib.mkIf (config?services.flatpak.packages) {
    remotes = lib.mkOptionDefault [{
      name = "gnome-nightly";
      location = "https://nightly.gnome.org/gnome-nightly.flatpakrepo";
    }];

    packages = [
      # Image
      "page.kramo.Sly"

      # Video
      "org.nickvision.tubeconverter"

      # Audio
      "io.github.revisto.drum-machine"

      # Reading
      "com.github.johnfactotum.Foliate"

      # Science
      "com.github.alexhuntley.Plots"

      # Office
      "com.belmoussaoui.Obfuscate"
      "com.github.muriloventuroso.pdftricks"
      "com.github.flxzt.rnote"
      "com.github.jeromerobert.pdfarranger"
      "garden.jamie.Morphosis"

      # Utilities
      "com.belmoussaoui.Decoder"
      "io.github.nokse22.minitext"
      "org.gnome.World.PikaBackup"
      "io.github.fabrialberio.pinapp"
      "re.sonny.Eloquent"

      # Customization
      "ca.desrt.dconf-editor"
      "app.drey.Damask"
      "com.github.GradienceTeam.Gradience"
      "com.github.tchx84.Flatseal"

      # Games
      "com.usebottles.bottles"
      "dev.tchx84.Gameeky"
      "dev.tchx84.Gameeky.ThematicPack.FreedomValley"
      "dev.tchx84.Gameeky.ThematicPack.Blasterman"
      "dev.tchx84.Gameeky.ThematicPack.Wackman"
      "net.zdechov.app.TransLines"
      "io.github.nokse22.ultimate-tic-tac-toe"

      # Programming
      { appId = "org.gnome.Builder.Devel"; origin = "gnome-nightly"; }
      "app.drey.Biblioteca"
    ];

    uninstallUnmanaged = true;
    update.onActivation = true;
    update.auto.enable = true;
  };

  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-lgc-plus
      noto-fonts-emoji
      cantarell-fonts
      twitter-color-emoji
      twemoji-color-font
      (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
    ];

    fontconfig = {
      useEmbeddedBitmaps = true;

      defaultFonts = {
        sansSerif = [ "Cantarell" "Noto Sans" ];
        monospace = [ "JetBrainsMono Nerd Font" ];
        emoji = [ "Twitter Color Emoji" "Noto Color Emoji" ];
      };
    };
  };

  nixpkgs.overlays = [
    # Version pins
    (final: prev: {
      # dolphin-emu = prev.dolphin-emu.overrideAttrs (super: {
      #   version = "2407";
      #   commit = "b92e354389bb7c0bd114a8631b8af110d3cb3a14";

      #   src = pkgs.fetchFromGitHub {
      #     owner = "dolphin-emu";
      #     repo = "dolphin";
      #     rev = "heads/refs/tags/${final.dolphin-emu.version}";
      #     hash = "sha256-8W4KyIj+rhDkWnQogjpzlEJVo3HJenfpWKimSyMGN7c=";
      #     fetchSubmodules = true;
      #   };

      #   cmakeFlags = [
      #     "-DDISTRIBUTOR=NixOS"
      #     "-DDOLPHIN_WC_BRANCH=${final.dolphin-emu.src.rev}"
      #     "-DDOLPHIN_WC_DESCRIBE=${final.dolphin-emu.version}"
      #     "-DDOLPHIN_WC_REVISION=${final.dolphin-emu.commit}"
      #   ];
      # });
    })
  ];
}
