{ pkgs, lib, ... }:

{
  # Networking
  networking.networkmanager.enable = true;

  networking.nameservers = [
    "9.9.9.9"
    "149.112.112.112"
    "1.1.1.1#one.one.one.one"
    "1.0.0.1#one.one.one.one"
    "2620:fe::fe"
    "2620:fe::9"
  ];

  services.resolved = {
    enable = false;
    dnssec = "true";
    domains = [ "~." ];
    fallbackDns = [
      "9.9.9.9"
      "149.112.112.112"
      "1.1.1.1#one.one.one.one"
      "1.0.0.1#one.one.one.one"
      "2620:fe::fe"
      "2620:fe::9"
    ];
    extraConfig = ''
      DNSOverTLS=yes
    '';
  };

  # Captive portals
  programs.captive-browser = {
    enable = true;
    interface = "wlp4s0";
  };

  # Firewall
  networking.firewall.allowedTCPPorts = [ 24872 8998 ];
  networking.firewall.allowedUDPPorts = [ 24872 8998 ];
  # networking.firewall.enable = false;

  # Network services
  #services.openssh.enable = true;

  #services.syncplay = {
  #  enable = true;
  #  port = 8998;
  #  extraArgs = [ "--password pouicbarilstepson123cassoulet" ];
  #};

  # Bluetooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;

  # KDE Connect
  programs.kdeconnect = {
    enable = true;
    package = lib.mkDefault pkgs.gnomeExtensions.gsconnect;
  };
}
