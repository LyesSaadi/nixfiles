{ ... }:

{
  imports =
    [
      ./networking.nix
      ./packages.nix
      ./system.nix
    ];
}
