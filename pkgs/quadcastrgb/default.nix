{
  stdenv,
  fetchFromGitHub,
  libusb1
}:

stdenv.mkDerivation (final: {
  pname = "quadcastrgb";
  version = "1.0.4";

  src = fetchFromGitHub {
    owner = "Ors1mer";
    repo = "QuadcastRGB";
    rev = "v${final.version}";
    hash = "sha256-PE7o4uty2csPYBQ+TlEnqB9m7hca2XMby9J15CWfwlU=";
  };

  # preBuild = ''
  #   export HOME=''$out
  # '';

  # preInstall = ''
  #   export BINDIR_INS=$out
  #   export MANDIR_INS=$out
  # '';
  
  installPhase = ''
    mkdir -p $out/bin $out/man
    make install BINDIR_INS=$out/bin MANDIR_INS=$out/man
  '';

  buildInputs = [ libusb1 ];
})
