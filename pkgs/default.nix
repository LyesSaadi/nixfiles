{ pkgs ? import <nixpkgs> { } }:

{
  quadcastrgb = pkgs.callPackage ./quadcastrgb { };
  ens-intel-unite = pkgs.callPackage ./ens-intel-unite { };
}
