{
  stdenv,
  # fetchTarball,
  # buildFHSEnv,
  lib,
  makeDesktopItem,
  copyDesktopItems,
  autoPatchelfHook,


  wayland,
  nwjs,
  glib,
  nss,
  nspr,
  cups,
  dbus,
  expat,
  libxkbcommon,
  cairo,
  pango,
  at-spi2-atk,
  libdrm,
  mesa,
  alsa-lib,
  swiftshader,
  xorg
}:

# let
#   dist = stdenv.mkDerivation (final: {
#     pname = "ens-intel-unite-dist";
#     version = "4.2.0.38";

#     src = fetchTarball {
#       # url = "https://unite.ens-paris-saclay.fr/Intel_Unite_Linux_Manual_Install_${final.version}-amd64.tar.bz2";
#       url = "file:///home/lyes/Téléchargements/Intel_Unite_Linux_Manual_Install_${final.version}-amd64.tar.bz2";
#       sha256 = "0nm77yzdsgy882s32si4rywaqh0kjc9g14x384kar1pcrp1lb0km";
#     };

#     installPhase = ''
#       mkdir -p $out/opt
#       rm -rf lib/
#       mv * $out/opt/
#     '';

#     dontFixup = true;
#   });
#   fhs = buildFHSEnv {
#     name = "ens-intel-unite-fhs";
#     runScript = "${dist}/opt/intel-unite-client";
#     targetPkgs = pkgs: (with pkgs; [
#       wayland
#       nwjs
#       glib
#       nss
#       nspr
#       cups
#       dbus
#       expat
#       libxkbcommon
#       cairo
#       pango
#       at-spi2-atk
#       libdrm
#       mesa
#       alsa-lib
#       swiftshader
#     ]) ++ (with pkgs.xorg; [
#       libxcb
#       libX11
#       libXcomposite
#       libXdamage
#       libXext
#       libXfixes
#       libXrandr
#     ]);
#   };
# in
stdenv.mkDerivation (final: {
    pname = "ens-intel-unite";
    # inherit (dist) version;
    version = "4.2.0.38";

    src = fetchTarball {
      # url = "https://unite.ens-paris-saclay.fr/Intel_Unite_Linux_Manual_Install_${final.version}-amd64.tar.bz2";
      url = "file:///home/lyes/Téléchargements/Intel_Unite_Linux_Manual_Install_${final.version}-amd64.tar.bz2";
      sha256 = "0nm77yzdsgy882s32si4rywaqh0kjc9g14x384kar1pcrp1lb0km";
    };

    # dontUnpack = true;

    nativeBuildInputs = [
      copyDesktopItems
      autoPatchelfHook
    ];

    buildInputs = [
      wayland
      nwjs
      glib
      nss
      nspr
      cups
      dbus
      expat
      libxkbcommon
      cairo
      pango
      at-spi2-atk
      libdrm
      mesa
      alsa-lib
      swiftshader
      xorg.libxcb
      xorg.libX11
      xorg.libXcomposite
      xorg.libXdamage
      xorg.libXext
      xorg.libXfixes
      xorg.libXrandr
    ];

    # sourceRoot = ".";

    installPhase = ''
      # cd intel-unite-client-${final.version}
      # cd source
      runHook preInstall
      # install -m755 -D intel-unite-client $out/bin/ens-intel-unite
      mkdir -p $out/bin
      # rm -rf lib/
      mv * $out/bin
      runHook postInstall
    '';

    # postInstall = ''
    #   mkdir -p $out/bin $out/share
    #   ln -s ${fhs}/bin/ens-intel-unite-fhs $out/bin/intel-unite
    # '';

    desktopItems = [
      (makeDesktopItem {
        name = "ens-intel-unite";
        exec = "ens-intel-unite";
        desktopName = "Intel Unite";
        genericName = "ENS Paris-Saclay's Intel Unite";
        icon = "/opt/icon128.png";
        categories = [
          "Network"
          "VideoConference"
        ];
        terminal = false;
        startupNotify = true;
      })
    ];

    meta = with lib; {
      mainProgram = "ens-intel-unite";
      description = "ENS Paris-Saclay's Intel Unite client packaged for NixOS";
      homepage = "https://unite.ens-paris-saclay.fr/";
      platforms = [ "x86_64-linux" ];
      license = licenses.unfree;
      sourceProvenance = with sourceTypes; [ binaryNativeCode ];
    };
})
