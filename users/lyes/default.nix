{ pkgs, home-manager, ... }:

{
  imports =
    [
      ./packages.nix
      home-manager.nixosModules.default
    ];

  users.users.lyes = {
    description = "Lyes Saadi";
    home = "/home/lyes";
    isNormalUser = true;
    initialHashedPassword = ""; # Set for vms and initial installations
    extraGroups = [ "wheel" "networkmanager" ];
    shell = pkgs.fish;
  };

  home-manager.useUserPackages = true;
  home-manager.useGlobalPkgs = true;
  home-manager.users.lyes = { pkgs, ... }: {
    imports =
      [
        ./home.nix
      ];
    
    home.username = "lyes";
    home.homeDirectory = "/home/lyes";

    home.stateVersion = "22.11";
  };
}
