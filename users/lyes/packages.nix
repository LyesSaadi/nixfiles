{ pkgs, nixpkgs-unstable, lib, config, ... }:

{
  # Packages
  users.users.lyes.packages = with pkgs; [
    # Commandline
    starship
    wl-clipboard
    sl
  ];
}
