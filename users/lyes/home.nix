{ ... }:

{
  # Git config
  programs.git = {
    enable = true;
    userName = "Lyes Saadi";
    userEmail = "dev@lyes.eu";
    signing = {
      key = "55A1D803917CF39A";
      signByDefault = true;
    };
    extraConfig = {
      color.ui = true;
      init.defaultBranch = "main";
      includeIf = {
        "gitdir:~/Documents/fedpkg/" = {
          path = "~/Documents/fedpkg/.gitconfig_include";
        };
      };
      push.followTags = true;
    };
  };

  # Protonup config
  home.sessionVariables = {
    STEAM_EXTRA_COMPAT_TOOLS_PATHS =
      "\${HOME}/.steam/root/compatibilitytools.d";
  };
}
