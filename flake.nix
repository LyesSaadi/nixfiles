{
  description = "NixOS Configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nix-flatpak.url = "github:gmodena/nix-flatpak/latest";
    zen-browser.url = "github:0xc000022070/zen-browser-flake";
    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, nixpkgs-unstable, nixos-hardware, nix-flatpak, disko, ... }@attrs: {
    nixosConfigurations = {
      lyes-pc = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = attrs;
        modules = [
          ./hosts/lyes-pc
          ./hosts/lyes-pc/disko-config.nix

          ./users/lyes

          ./modules
          ./modules/desktop
          ./modules/gaming
          # ./modules/nvidia
          # ./modules/specialisations/old-kernel.nix
          # ./modules/specialisations/gaming.nix
          ./modules/sway

          nix-flatpak.nixosModules.nix-flatpak
          nixos-hardware.nixosModules.framework-16-7040-amd
          disko.nixosModules.disko
        ];
      };

      desktop-iso = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = attrs;
        modules = [
          ./hosts/iso

          ./modules
          ./modules/desktop
          ./modules/gaming
          ./modules/nvidia

          nix-flatpak.nixosModules.nix-flatpak
          "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
          "${nixpkgs}/nixos/modules/installer/cd-dvd/channel.nix"
        ];
      };
    };
  };
}
