{ pkgs, ... }:

{
  imports = [
      ./hardware-configuration.nix
    ];

  # Networking
  networking = {
    hostName = "lyes-pc";
    hostId = "ed183b8f";
  };

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_6_12;

  # Boot
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 10;
  # boot.loader.efi.canTouchEfiVariables = true;
  # boot.kernelParams = [ "quiet" ];
  boot.initrd.systemd = {
    enable = true;
    # network.enable = true;
  };

  # Reboot Timeout
  systemd.extraConfig = ''
    DefaultTimeoutStopSec=10s
  '';

  # Swap
  zramSwap.enable = true;

  # Wake Up issues
  services.udev.extraRules =
    ''
      ACTION=="add", SUBSYSTEM=="acpi", DRIVERS=="button", ATTRS{hid}=="PNP0C0D", ATTR{power/wakeup}="disabled"
      ACTION=="add", SUBSYSTEM=="i2c", DRIVERS=="i2c_hid_acpi", ATTRS{name}=="PIXA3854:00", ATTR{power/wakeup}="disabled"

      ACTION=="add", SUBSYSTEM=="usb", DRIVERS=="usb", ATTRS{idVendor}=="32ac", ATTRS{idProduct}=="0018", ATTR{power/wakeup}="disabled", ATTR{driver/1-1.1.1.4/power/wakeup}="disabled"
      ACTION=="add", SUBSYSTEM=="usb", DRIVERS=="usb", ATTRS{idVendor}=="32ac", ATTRS{idProduct}=="0014", ATTR{power/wakeup}="disabled", ATTR{driver/1-1.1.1.4/power/wakeup}="disabled"
    '';

  # Optimisation

  # nixpkgs.hostPlatform = {
  #   gcc.arch = "tigerlake";
  #   gcc.tune = "tigerlake";
  #   system = "x86_64-linux";
  # };

  # nix.settings.system-features = [ "gccarch-tigerlake" "nixos-test" "benchmark" "big-parallel" "kvm" ];

  # # Increasing the limit of files to help with compilation
  # security.pam.loginLimits = [{
  #   domain = "*";
  #   type = "soft";
  #   item = "nofile";
  #   value = "8192";
  # }];

  # # Resources Limit
  # nix.settings = {
  #   max-jobs = 7;
  #   cores = 2;
  # };

  # # OOM configuration
  # systemd = {
  #   # Create a separate slice for nix-daemon that is
  #   # memory-managed by the userspace systemd-oomd killer
  #   slices."nix-daemon".sliceConfig = {
  #     ManagedOOMMemoryPressure = "kill";
  #     ManagedOOMMemoryPressureLimit = "50%";
  #   };
  #   services."nix-daemon".serviceConfig.Slice = "nix-daemon.slice";

  #   # If a kernel-level OOM event does occur anyway,
  #   # strongly prefer killing nix-daemon child processes
  #   services."nix-daemon".serviceConfig.OOMScoreAdjust = 1000;

  #   services."nix-daemon".serviceConfig.MemoryHigh = "16G";
  #   services."nix-daemon".serviceConfig.MemoryMax = "20G";
  # };

  # Disabling failing test
  # nixpkgs.overlays = [ (final: prev: {
  # #   orc = prev.orc.overrideAttrs (_: { doCheck = false; });
  # #   gsl = prev.gsl.overrideAttrs (_: { doCheck = false; });
  #   fprintd = prev.fprintd.overrideAttrs (super: {
  #     # doCheck = false;
  #     # buildInputs = super.buildInputs or [ ] ++ (with pkgs; [ libpam-wrapper (pkgs.python3.withPackages (python-pkgs: [ python-pkgs.pycairo python-pkgs.dbus-python python-pkgs.python-dbusmock ])) ]);
  #     mesonCheckFlags = [
  #       # PAM related checks are timing out
  #       "--no-suite" "fprintd"
  #     ];
  #   });
  #   libsrtp = prev.libsrtp.overrideAttrs (_: {
  #     mesonFlags = [
  #       "-Dcrypto-library=openssl"
  #       "-Dcrypto-library-kdf=disabled"
  #       "-Ddoc=disabled"
  #       "-Dtests=disabled"
  #     ];
  #   });
  # #   # haskellPackages.crypton = pkgs.haskell.lib.overrideCabal prev.crypton (_: { doCheck = false; });
  # #   # haskellPackages.cryptonite = pkgs.haskell.lib.overrideCabal prev.cryptonite (_: { doCheck = false; });
  # }) ];

  # nixpkgs.config.packageOverrides = pkgs: {
  #   haskellPackages = pkgs.haskellPackages.override {
  #     overrides = hsSelf: hsSuper: {
  #       crypton = pkgs.haskell.lib.overrideCabal hsSuper.crypton (_: { doCheck = false; });
  #       cryptonite = pkgs.haskell.lib.overrideCabal hsSuper.cryptonite (_: { doCheck = false; });
  #       crypton-x509-validation = pkgs.haskell.lib.overrideCabal hsSuper.crypton-x509-validation (oa: { doCheck = false; });
  #       tls = pkgs.haskell.lib.overrideCabal hsSuper.tls (oa: { doCheck = false; });
  #     };
  #   };
  # };
}
