{ pkgs, lib, ... }:
let
  calamares-auto-start = pkgs.makeAutostartItem { name = "io.calamares.calamares"; package = pkgs.calamares-nixos; };
in {
  # VMs
  services.spice-vdagentd.enable = true;
  services.qemuGuest.enable = true;

  # Calamares for graphical installation
  environment.systemPackages = with pkgs; [
    libsForQt5.kpmcore
    calamares-nixos
    calamares-auto-start
    calamares-nixos-extensions
  ];

  networking.hostName = "lyes-iso";
  networking.wireless.enable = lib.mkForce false;

  # services.displayManager.autoLogin.user = lib.mkForce "lyes";
}
